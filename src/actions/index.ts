export function addTodo(label: string) {
    return ({
        type: 'ADD_TODO',
        label: label,
        done: false
    })
}

export function toggleTodo(index: number) {
    return ({
        type: 'TOGGLE_TODO',
        index: index
    });
}

export function toggleHideDone() {
    return ({
        type: 'TOGGLE_HIDE_DONE'
    });
}