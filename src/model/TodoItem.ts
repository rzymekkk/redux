export interface TodoItem {
    done: boolean;
    label: string;
}