import * as React from "react";
import TodoList from "./containers/TodoList";
import TodoAdd from "./containers/TodoAdd";
import TodoFilter from "./containers/TodoFilter";


export class App extends React.Component<{},{}> {
    render() {
        return <div>
            <TodoList/>
            <TodoAdd/>
            <br/>
            <TodoFilter/>
        </div>;
    }
}
