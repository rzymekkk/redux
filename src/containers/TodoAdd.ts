import {addTodo} from "../actions/index";
import {Dispatch} from "redux";
import {connect} from "react-redux";
import {TodoAdd} from "../components/TodoAdd";

function mapDispatchToProps(dispatch: Dispatch<any>) {
    return {
        onSubmit(label: string) {
            dispatch(addTodo(label))
        }
    }
}

const TodoAddContainer = connect(
    undefined,
    mapDispatchToProps
)(TodoAdd);

export default TodoAddContainer;
