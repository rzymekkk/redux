import {TodoList} from "../components/TodoList";
import {toggleTodo} from "../actions/index";
import {Dispatch} from "redux";
import {connect} from "react-redux";
import {State} from "../reducers/index";

function getTodos(state: State) {
    const items = state.todos.items;
    if (state.filter.hideDone) {
        return items.filter(item => !item.done)
    } else {
        return items;
    }
}

function mapStateToProps(state: State) {
    return {
        items: getTodos(state)
    }
}

function mapDispatchToProps(dispatch: Dispatch<any>) {
    return {
        onDoneToggle(index: number) {
            dispatch(toggleTodo(index))
        }
    }
}

const TodoListContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(TodoList);

export default TodoListContainer;
