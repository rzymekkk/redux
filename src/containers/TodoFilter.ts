import {toggleHideDone} from "../actions/index";
import {Dispatch} from "redux";
import {connect} from "react-redux";
import {TodoFilter} from "../components/TodoFilter";
import {State} from "../reducers/index";

function mapStateToProps(state: State) {
    return {
        value: state.filter.hideDone
    }
}

function mapDispatchToProps(dispatch: Dispatch<any>) {
    return {
        onChange() {
            dispatch(toggleHideDone())
        }
    }
}

const TodoFilterContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(TodoFilter);

export default TodoFilterContainer;
