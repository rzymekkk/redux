import {TodoItem} from "../model/TodoItem";

export interface TodoState {
    items: TodoItem[]
}

const defaultState = {
    items: [
        {label: 'Submit sulu', done: false},
        {label: 'Do code review', done: false},
        {label: 'Prepare RzymekON', done: true}
    ]
};

export function todos(state: TodoState = defaultState, action: any): TodoState {
    switch (action.type) {
        case 'ADD_TODO': {
            const newItem = {
                label: action.label,
                done: action.done
            };
            return {
                items: state.items.concat([newItem])
            }
        }
        case 'TOGGLE_TODO': {
            const newItems = state.items.map((todo, index) => {
                if (index == action.index) {
                    return toggleTodo(todo);
                } else {
                    return todo;
                }
            });
            return {
                items: newItems
            }
        }
        default:
            // return the previous state for any unknown action.
            return state;
    }
}

function toggleTodo(todo: TodoItem): TodoItem {
    return Object.assign({}, todo, {
            done: !todo.done
        }
    ) as TodoItem
}