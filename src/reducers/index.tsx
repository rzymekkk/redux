import {combineReducers} from 'redux'
import {todos, TodoState} from "./todos";
import {filter, TodoFilterState} from "./filter";

export interface State {
    todos: TodoState,
    filter: TodoFilterState
}

const rootReducer = combineReducers({
    todos,
    filter
});

export default rootReducer;
