export interface TodoFilterState {
    hideDone: boolean
}

const defaultState = {
    hideDone: false
};

export function filter(state: TodoFilterState = defaultState, action: any): TodoFilterState {
    switch (action.type) {
        case 'TOGGLE_HIDE_DONE': {
            return {
                hideDone: !state.hideDone
            }
        }
        default:
            // return the previous state for any unknown action.
            return state;
    }
}
