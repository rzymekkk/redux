import * as React from "react";
import * as ReactDOM from "react-dom";
import {App} from "./App";
import {createStore} from "redux";
import {Provider} from "react-redux";
import rootReducer from "./reducers/index";

const store = createStore(
    rootReducer,
    call(window['__REDUX_DEVTOOLS_EXTENSION__'])
);

ReactDOM.render(
    <Provider store={store}>
        <App/>
    </Provider>,
    document.getElementById("root")
);


function call(func: any): any {
    return func && func();
}