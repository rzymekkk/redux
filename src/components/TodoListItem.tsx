import * as React from "react";
import {TodoItem} from "../model/TodoItem";

interface TodoListItemProps {
    item: TodoItem;
    onChange(): void;
}
export class TodoListItem extends React.Component<TodoListItemProps,{}> {
    render() {
        return <li>
            <label style={this.getStyle()}>
                <input checked={this.props.item.done}
                       type="checkbox"
                       onChange={e => this.props.onChange()}/>
                {this.props.item.label}
            </label>
        </li>;
    }

    private getStyle(): React.CSSProperties {
        if (this.props.item.done) {
            return ({
                textDecoration: 'line-through'
            });
        } else {
            return undefined;
        }
    }
}

