import * as React from "react";

interface TodoAddProps {
    onSubmit(value: string): void;
}
export class TodoAdd extends React.Component<TodoAddProps,{}> {
    private handleSubmit(e: React.FormEvent<any>) {
        e.preventDefault();
        const input = this.refs['input'] as HTMLInputElement;
        this.props.onSubmit(input.value);
        input.value = '';
    }

    render() {
        return <form onSubmit={e => this.handleSubmit(e)}>
            <label>New TODO:
                <input ref="input" type="text" size={80}/>
            </label>
        </form>
    }
}
