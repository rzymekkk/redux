import * as React from "react";

interface TodoFilterProps {
    value: boolean;
    onChange(): void;
}
export class TodoFilter extends React.Component<TodoFilterProps,{}> {
    render() {
        return <label>
            <input type="checkbox"
                   checked={this.props.value}
                   onChange={e=>this.props.onChange()}/>
            Hide finished
        </label>
    }
}
