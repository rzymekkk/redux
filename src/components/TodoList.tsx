import * as React from "react";
import {TodoItem} from "../model/TodoItem";
import {TodoListItem} from "./TodoListItem";

interface TodoListProps {
    items: TodoItem[],
    onDoneToggle(index: number): void;
}

export class TodoList extends React.Component<TodoListProps,{}> {
    render() {
        return <ul>
            {this.props.items.map((item, index) =>
                <TodoListItem item={item}
                              onChange={() => this.props.onDoneToggle(index)}
                              key={index}/>
            )}
        </ul>;
    }
}
